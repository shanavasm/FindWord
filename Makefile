src := src/ui/*.java src/findword/*.java
build := bin

.PHONY: all clean

all: clean run

init:
	mkdir bin
	cp -r src/db bin/

compile: init
	javac -d $(build) $(src)

run: compile
	java -cp $(build) ui.FindWordUi

clean:
	rm -r bin
